defmodule PersistentBugger do
  def persistence(n) do
    to_string(n)
    |> persist_string(0)
  end

  defp persist_string(string, count) do
    case String.length(string) do
      1 -> count
      _ -> string
           |> String.split("", trim: true)
           |> Enum.map(
                fn s ->
                  {i, _} = Integer.parse(s)
                  i
                end
              )
           |> Enum.reduce(1, fn i, acc -> acc * i end)
           |> to_string()
           |> persist_string(count + 1)
    end
  end
end

p = &PersistentBugger.persistence/1
IO.inspect(p.(39))
IO.inspect(p.(999))
IO.inspect(p.(4))
https://www.codewars.com/kata/persistent-bugger/train/elixir